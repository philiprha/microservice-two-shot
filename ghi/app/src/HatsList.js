import React, { useEffect, useState} from 'react';



function HatsList(props) {
    // const [locations, setLocations ] = useState([]);
    const [hats, setHats] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/location/');
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
        }
    }

    useEffect(()=>{
    getData()
    }, [])



    // const getLocationData = async () => {

    //     const url ='http://localhost:8100/api/locations/';
    //     const response = await fetch(url);

    //     if (response.ok) {
    //         const data = await response.json();
    //         setLocations(data.locations);
    //     };
    // }

    
    
    const handleDelete = async (id) => {
        const hatUrl = "http://localhost:8090/api/hats/"+id;
        fetch(hatUrl, {method: 'DELETE'})
        .then((r) => {
            if (!r.ok) {
                throw new Error('Something went wrong');
            }
            return r.json();
        
        })
        const newHats = hats.filter((hat) => hat.id !== id);

        setHats(newHats);
        



        // setHats(oldValues => {
        //     return oldValues.filter(hat => hat.id !== id)
        // });

    }
    useEffect(()=>{
        getData()
        }, [])


    
    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Location</th>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Picture URL</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {hats.map( hat => {
                    return(
                        <tr key={hat.id}>
                            
                            <td>{ hat.location.id }</td>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.picture_url }</td>
                            <td>
                                <button onClick = {() => handleDelete(hat.id)} className="btn btn-primary">Delete</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    )
}

export default HatsList;
