import React, {useEffect, useState} from 'react';

function HatForm (props) {
    const [locations, setLocations ] = useState([]);
    const [formData, setFormData ] = useState({
        fabric: '',
        style_name: '',
        color : '',
        location : '',  
        picture_url: '',
    });

    const getData = async () => {

        const url ='http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        };
    };


    const formDataChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };


    useEffect(() => {
        getData();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        console.log(formData)
        const response = await fetch(hatUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                fabric: '',
                style_name:'',
                color:'',
                picture_url:'',
                location:'',
            });
        };
    };


    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                <div className="form-floating mb-3">
                <input onChange={formDataChange}
                placeholder="fabric"
                value = {formData.fabric}
                required type="text"
                name="fabric"
                id="fabric"
                className="form-control" />
                    <label htmlFor="fabric">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={formDataChange}
                    placeholder="style_name"
                    value = {formData.style_name}
                    required type="text"
                    name="style_name"
                    id="style_name"
                    className="form-control"/>
                    <label htmlFor="style_name">Style</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={formDataChange}
                    placeholder="Color"
                    value = {formData.color}
                    required type="text"
                    name="color"
                    id="color"
                    className="form-control"/>
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={formDataChange} 
                    placeholder="picture_url" 
                    value = {formData.picture_url}
                    required type="text" 
                    name="picture_url" 
                    id="picture_url" 
                    className="form-control"/>
                    <label htmlFor="pictur_url">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select value={formData.location} onChange= {formDataChange} required name="location" id="location" className="form-select">
                        <option value="">Choose a Location</option>
                        {locations.map(location => {
                            return (
                            <option key={location.id} value={location.href}>
                                {location.closet_name}
                            </option>

                        );
                    })}


                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    )};

    export default HatForm;
