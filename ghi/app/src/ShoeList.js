import { useEffect, useState } from 'react';

function ShoesList() {
  const [shoes, setShoes] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/bin/');

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    }
  }

  useEffect(()=>{
    getData()
  }, [])



  const handleDelete = async (id) => {
    // id.preventDefault();

    const shoeurl = 'http://localhost:8080/api/shoes/' + id;
    fetch(shoeurl, {method: 'DELETE'})
    .then((r) => {
      if (!r.ok) {
        throw new Error('NOOOO');
      }
      return r.json()
    })

    const newShoes = shoes.filter((shoe) => shoe.id !==id);

    setShoes(newShoes)

  }





  return (
    <div id="root">
        <table className="table table-striped">
            <thead>
                <tr>
                <th>Bin</th>
                <th>Manufacturer</th>
                <th>Model</th>
                <th>Color</th>
                <th>Picture URL</th>
                </tr>
            </thead>
            <tbody>
                {shoes.map(shoe => {
                return (
                    <tr key={shoe.id}>
                    <td>{shoe.bin.id}</td>
                    <td>{shoe.manufacturer}</td>
                    <td>{ shoe.model_name }</td>
                    <td>{ shoe.color }</td>
                    <td>{shoe.picture_url}</td>
                    <td>
                      <button onClick={() => handleDelete(shoe.id)}>Delete</button>
                    </td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    </div>
  );
}

export default ShoesList;
