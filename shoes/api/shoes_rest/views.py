from django.shortcuts import render
from .models import Shoe, BinVO
from django.http import JsonResponse
import json
from common.json import ModelEncoder
from shoes_project.acls import get_photo
# from wardrobe.api.wardrobe_api.views import BinEncoder
from django.views.decorators.http import require_http_methods
# from wardrobe.api.wardrobe_api.models import Bin

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["id", "import_href",]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "model_name",
        "color",
        "picture_url",
        "id",
        "manufacturer",
        "bin"
    ]
    encoders = {"bin": BinVODetailEncoder()}
    # def get_extra_data(self, o):
    #     return {"bin": o.bin.name}

class ShoeDetailEncoder(ModelEncoder):
        model = Shoe
        properties = [
            "model_name",
            "color",
            "picture_url",
            "manufacturer",
            "bin",
        ]
        encoders = {"bin": BinVODetailEncoder()}

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message":"Invalid bid id"},
                status=400
            )
        # photo = get_photo(content["color"], content["model_name"], content["manufacturer"])
        # content.update(photo)
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
@require_http_methods(["DELETE", "GET"])
def api_show_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.object.get(id=None)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
