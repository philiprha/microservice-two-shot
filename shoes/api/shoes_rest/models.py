from django.db import models

# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)

# Create your models here.
class Shoe(models.Model):
    model_name = models.CharField(max_length = 200)
    manufacturer = models.CharField(max_length=200, default="")
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    bin = models.ForeignKey(

        BinVO,
        related_name = "shoes",
        on_delete=models.CASCADE,
    )
