from django.urls import path
from .views import api_list_shoes, api_show_shoe

urlpatterns = [
    path("shoes/", api_list_shoes, name='api_create_shoes'),
    path("shoes/<int:id>/",  api_show_shoe, name="api_show_shoe"),
    path("bin/<bin_vo_id>/shoes", api_list_shoes, name="list_shoes"),
    path("bin/", api_list_shoes, name="show_all_shoes")
]
