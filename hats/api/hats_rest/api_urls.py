from django.urls import path
from .views import api_show_hat, api_list_hat

urlpatterns = [
    path("location/<int:location_vo_id>/hats", api_list_hat, name="api_list_hats"),
    path("hats/", api_list_hat, name="api_create_hat"),
    path("hats/<id>/", api_show_hat,name="api_show_hat"),
    path("location/", api_list_hat, name="show_all_hats")
]