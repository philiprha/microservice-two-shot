from common.json import ModelEncoder
from django.http import JsonResponse
from .models import Hat, LocationVO
import json
from django.views.decorators.http import require_http_methods
from hats_project.acls import get_photo

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "id",
        ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
        
    ]
    encoders = {"location": LocationVODetailEncoder()}
   

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
        
    ]
    encoders = {"location": LocationVODetailEncoder()}




@require_http_methods(["GET", "POST"])
def api_list_hat(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
            
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        # picture_url = get_photo(content["color"],content["style_name"],content["fabric"])
        # content.update(picture_url)

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def api_show_hat(request, id=None):
    if request.method =="GET":
        hat = Hat.objects.get(id=id)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method =="DELETE":
        count, _ = Hat.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

