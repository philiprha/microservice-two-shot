# Generated by Django 4.0.3 on 2024-01-31 17:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='hat',
            name='fabric',
            field=models.CharField(default='', max_length=200),
        ),
        migrations.AddField(
            model_name='hat',
            name='picture_url',
            field=models.URLField(null=True),
        ),
        migrations.AddField(
            model_name='hat',
            name='style_name',
            field=models.CharField(default='', max_length=200),
        ),
    ]
